$(document).ready(function(){
    var width = $(document).width();
    var height = $(document).height();       
    $("#graph-container").css("width", width);
    $("#graph-container").css("height", height);
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $(".zoom").css('display', 'none');
    }
    $("#zoom-in").click(function() {
        var zoom = parseFloat($(".row-item").css("zoom"));
        if (zoom >= 2) {
            return;
        }
        zoom += 0.1;
        $(".row-item").css("zoom", zoom);        
    });

    $("#zoom-out").click(function() {
        var zoom = parseFloat($(".row-item").css("zoom"));
        if (zoom <= 0.4) {
            return;
        }
        zoom -= 0.1;
        $(".row-item").css("zoom", zoom);
    });

    var curDown = false,
      curYPos = 0,
      curXPos = 0;
    $("#graph-container").mousemove(function(m){
        if(curDown === true){            
            $("#graph-container").scrollTop($("#graph-container").scrollTop() + (curYPos - m.pageY)); 
            $("#graph-container").scrollLeft($("#graph-container").scrollLeft() + (curXPos - m.pageX));
        }
    });
    
    $("#graph-container").mousedown(function(m){
        $('#graph-container').css('cursor', 'grab');        
        curDown = true;
        curYPos = m.pageY;
        curXPos = m.pageX;
    });
    
    $("#graph-container").mouseup(function(){
        $('#graph-container').css('cursor', 'default');
        curDown = false;
    });

    $(".action-root").click(function() {
        var id = $(this).attr("id");
        var idChild = $("#cert-details-" + id).find("input:button").attr("id");
        if (!$("#cert-details-" + idChild).is(":hidden")) {
            $("#cert-details-" + idChild).slideToggle("slow");            
        }        
        $("#cert-details-" + id).slideToggle("slow");        
    });
    
    $(".action-chain").click(function() {
        var id = $(this).attr("id");       
        $("#cert-details-" + id).slideToggle("slow");        
    });

    $.fn.extend({
        treePub: function () {
            var tree = $(this);
            //tree.addClass("tree-publish");
            tree.find('li').has("ul.ul-child").each(function () {
                var branch = $(this); 
                branch.addClass('branch');
                branch.on('click', function (e) {                    
                    if (this == e.target) {                        
                        if ($(this).children().children().is(":hidden")) {                            
                            $(this).children().removeClass('fa-plus-circle');
                            $(this).children().addClass('fa-minus-circle');
                        } else {
                            $(this).children().removeClass('fa-minus-circle');
                            $(this).children().addClass('fa-plus-circle');
                        }   
                        $(this).children().children().slideToggle("slow");                                     
                    }
                })
                //branch.children().children().slideToggle("slow");
            });
            
          tree.find('.branch .indicator').each(function(){
            $(this).on('click', function () {
                $(this).closest('li').click();
            });
          });
            
            tree.find('.branch>a').each(function () {
                $(this).on('click', function (e) {
                    $(this).closest('li').click();
                    e.preventDefault();
                });
            });
        }
    });

    $.fn.extend({
        treeDevice: function () {
            var tree = $(this);
            tree.addClass("tree-dev");
            tree.find('li').has("ul.ul-child-device").each(function () {
                var branchDev = $(this); 
                branchDev.addClass('branch-dev');
                branchDev.on('click', function (e) {                    
                    if (this == e.target) {                        
                        if ($(this).children().children().is(":hidden")) {                                                        ;
                            $(this).children().removeClass('fa-plus-circle');                                                        
                            $(this).children().addClass('fa-minus-circle');                            
                        } else {
                            $(this).children().removeClass('fa-minus-circle');                            
                            $(this).children().addClass('fa-plus-circle');                            
                        }   
                        $(this).children().children().slideToggle("slow");                                     
                    }                    
                })
                //branchDev.children().children().slideToggle("slow");
            });
            
          tree.find('.branch-dev .indicator').each(function(){
            $(this).on('click', function () {
                $(this).closest('li').click();
            });
          });
            
            tree.find('.branch-dev >a').each(function () {
                $(this).on('click', function (e) {                    
                    $(this).closest('li').click();
                    e.preventDefault();
                });
            });
        }
    });
    $('.publish-tree').treePub();    

    $('.add-plugin').bind('click', function(){
        var plugin = $(this).attr('id');
        var content = $('#template-plugin').children('li').clone();        
        content.find('div.connector-box').prepend(plugin);
        content.find('ul.publish-dev').treeDevice();        
        content.find('div.container-add-device').bind('click', function(){
            var dev = $('#template-device').children('li').clone();
            dev.find('.select-search').selectpicker('render');
            dev.find('.btn-publish').bind('click', function(){
                setTimeout(function() { 
                    dev.find('.select-search').find('button').css("background", "#FFD39B");
                }, 300);
                setTimeout(function() { 
                    dev.find('.select-search').find('button').css("background", "#99cc33");                    
                    dev.find('.btn-publish').addClass('hidden');  
                    dev.find('.select-search').prop('disabled', true);                
                    dev.find('.select-search').selectpicker('refresh');
                }, 5000);
            });
            $(dev).insertBefore($(this).parent());
        });       
        var idForm = $(this).parent().parent().parent().parent().find('>li').length;
        $(content).find('div.collapse').attr('id', plugin + '-' + idForm);
        $(content).find('div.collapse').collapse('hide');
        $(content).find('div.collapse').find('form').on('submit', function(){            
            //var values = $(this).serialize();            
            //$(this).trigger("reset");
            $(content).find('div.collapse').collapse('toggle');
            return false;
        });
        $(content).find('div.connector-action').find('input').bind('click', function(){
            $(content).find('div.collapse').collapse('toggle');
        });
        //console.log();      
        $(content).insertBefore( $(this).parent().parent().parent().parent().parent().find('.container-add-publish').parent());        
    });       
});